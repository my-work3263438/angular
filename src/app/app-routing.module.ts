import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { EmployeeService } from './employee.service';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { ObjectComponent } from './object/object.component';

const routes: Routes = [
  {path : 'create-employee',component :CreateEmployeeComponent},
  {path:'employees',component:EmployeeListComponent},
  {path:"",redirectTo:'employee',pathMatch:'full'},
  {path:'create-student',component:ObjectComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
