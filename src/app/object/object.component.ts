// object.component.ts
import { Component } from '@angular/core';


import { from} from 'rxjs';
import { toArray ,catchError} from 'rxjs/operators';




import { Student } from '../student';
import { StudentAddress } from '../student-address';
import { StudentService } from '../student.service';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
})
export class ObjectComponent {
  constructor(private studentService: StudentService, private addressService: AddressService) {}

  async createAndSaveObjects() {
    try {
      // Create a task
      const student: Student = {
        id: 0,
        firstName:'Raj',
        lastName: 'sharma',
         gender: 'male',
      };

      // Create a user
      const studentAddress: StudentAddress = {
        id: 0,
    streetNumber: '105',
    city:'Indore',
    country:'India'

      };

      // Create and save the task
      const createdStudent = await from(this.studentService.createStudent(student))
      .pipe(
        toArray(),
        catchError(error => {
          console.error('Error:', error);
          throw error; 
        })
      )
      .toPromise();


      // Create and save the user
      const createdAddress = await this.addressService.createAddress(studentAddress).toPromise();

      console.log('Student saved:', createdStudent);
      console.log('Address saved:', createdAddress);
    } catch (error) {
      console.error('Error:', error);
    }
  }
}

