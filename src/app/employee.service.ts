import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/api/createEmployee';

  private baseUrl2= 'http://localhost:8080/api/getEmployees';

  constructor(private http: HttpClient) { }

  createEmployee(employee: Employee): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, employee);
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.baseUrl2}`);
  }

  

  

  
}

