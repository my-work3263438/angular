import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentAddress } from './student-address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private apiUrl = 'http://localhost:8081/api/student/saveAddress';

  constructor(private http: HttpClient) {}

  createAddress(address:StudentAddress): Observable<StudentAddress> {
    return this.http.post<StudentAddress>(`${this.apiUrl}`, address);
  }
}
